package com.uol.hodassistant.interfaces;

import com.uol.hodassistant.classes.Users;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserApiInterface {

    @POST("read.php")
    Call<List<Users>> getUsers();


    @FormUrlEncoded
    @POST("write.php")
    Call<Users> insertUsers(
            @Field("username") String username,
            @Field("email") String email,
            @Field("password") String password,
            @Field("deptID") String deptID);

    /*
    @FormUrlEncoded
    @POST("update_pet.php")
    Call<Users> updatePet(
            @Field("key") String key,
            @Field("id") int id,
            @Field("name") String name,
            @Field("species") String species,
            @Field("breed") String breed,
            @Field("gender") int gender,
            @Field("birth") String birth,
            @Field("picture") String picture);

    @FormUrlEncoded
    @POST("delete_pet.php")
    Call<Users> deletePet(
            @Field("key") String key,
            @Field("id") int id,
            @Field("picture") String picture);

    @FormUrlEncoded
    @POST("update_love.php")
    Call<Users> updateLove(
            @Field("key") String key,
            @Field("id") int id,
            @Field("love") boolean love);*/

}
