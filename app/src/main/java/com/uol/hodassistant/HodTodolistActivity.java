package com.uol.hodassistant;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

public class HodTodolistActivity extends AppCompatActivity {

    private FloatingActionButton mAddButton;
    private Toolbar mToolbar;
    ListView todoListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hod_todolist);

        mAddButton = (FloatingActionButton) findViewById(R.id.addTodoList);
        mToolbar = (Toolbar) findViewById(R.id.todoListToolbar);

        //setSupportActionBar(mToolbar);
        //mToolbar.setTitle("ToDo List");

        todoListView = (ListView) findViewById(R.id.list);
        View emptyView = findViewById(R.id.empty_view);
        todoListView.setEmptyView(emptyView);

        //mCursorAdapter = new AlarmCursorAdapter(this, null);
        //todoListView.setAdapter(mCursorAdapter);

        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(v.getContext(), HodTodolistAddActivity.class);
                startActivity(intent1);
            }
        });

    }
}
