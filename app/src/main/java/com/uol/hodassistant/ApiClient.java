package com.uol.hodassistant;



import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by haerul on 15/03/18.
 */

class ApiClient {

    private static  String BASE_URL = null;
    private static Retrofit retrofit;

    static Retrofit getApiClient(String key){

        if(key.equals("User"))
        {
            BASE_URL= "https://www.hodassisstant.xyz/api/User/";
        }
        else   if(key.equals("Department"))
        {
            BASE_URL= "http://192.168.10.9/hodApi/api/Department/";
        }

        if (retrofit==null){

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }

}
