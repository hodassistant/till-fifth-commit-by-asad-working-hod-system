package com.uol.hodassistant;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class HomeTeacherFragment extends Fragment {

    CardView homeTeacherCard01, homeTeacherCard02, homeTeacherCard03, homeTeacherCard04, homeTeacherCard05, homeTeacherCard06;

    public HomeTeacherFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_teacher, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        homeTeacherCard01 = (CardView)view.findViewById(R.id.homeTeacher01);
        homeTeacherCard02 = (CardView)view.findViewById(R.id.homeTeacher02);
        homeTeacherCard03 = (CardView)view.findViewById(R.id.homeTeacher03);
        homeTeacherCard04 = (CardView)view.findViewById(R.id.homeTeacher04);
        homeTeacherCard05 = (CardView)view.findViewById(R.id.homeTeacher05);
        homeTeacherCard06 = (CardView)view.findViewById(R.id.homeTeacher06);

        homeTeacherCard01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        homeTeacherCard05.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent5 = new Intent(getActivity(),TeacherLeaveRequestActivity.class);
                startActivity(intent5);
            }
        });

    }
}
