package com.uol.hodassistant.classes;

import com.google.gson.annotations.SerializedName;

public class Users {


    @SerializedName("userID")
    public int id;
    @SerializedName("username")
    public String user_name;
    @SerializedName("email")
    public String user_email;
    @SerializedName("password")
    public String user_password;
    @SerializedName("deptID")
    public int dep_id;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public int getDep_id() {
        return dep_id;
    }

    public void setDep_id(int dep_id) {
        this.dep_id = dep_id;
    }
}
