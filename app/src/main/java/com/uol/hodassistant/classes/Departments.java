package com.uol.hodassistant.classes;

import com.google.gson.annotations.SerializedName;

public class Departments {

    @SerializedName("deptID")
    public int id;
    @SerializedName("deptName")
    public String name;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
