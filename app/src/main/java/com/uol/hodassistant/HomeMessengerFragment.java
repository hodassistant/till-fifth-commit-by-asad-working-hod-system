package com.uol.hodassistant;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class HomeMessengerFragment extends Fragment {

    CardView homeMessengerCard01, homeMessengerCard02, homeMessengerCard03;

    public HomeMessengerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_messenger, container, false);
    }


    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        homeMessengerCard01 = (CardView)view.findViewById(R.id.homeMessenger01);
        homeMessengerCard02 = (CardView)view.findViewById(R.id.homeMessenger02);
        homeMessengerCard03 = (CardView)view.findViewById(R.id.homeMessenger03);


        homeMessengerCard01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(),StudentComplainActivity.class);
                startActivity(intent1);
            }
        });
    }
}
