//Umar is here
package com.uol.hodassistant;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.uol.hodassistant.classes.Departments;
import com.uol.hodassistant.classes.Users;
import com.uol.hodassistant.interfaces.DepartmentApiInterface;
import com.uol.hodassistant.interfaces.UserApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private EditText mEmail, mPassword;
    private Button mLoginBtn, mExitBtn;
    private Spinner mDomain;
    UserApiInterface apiInterface;
    DepartmentApiInterface departmentApiInterface;
    private List<Users> Userss;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        apiInterface = ApiClient.getApiClient("User").create(UserApiInterface.class);
        departmentApiInterface=ApiClient.getApiClient("Department").create(DepartmentApiInterface.class);
        mEmail = (EditText) findViewById(R.id.loginEmail);
        mPassword = (EditText)findViewById(R.id.loginPassword);
        mDomain = (Spinner)findViewById(R.id.loginDomain);
        mLoginBtn = (Button)findViewById(R.id.loginSignIn);
        mExitBtn = (Button)findViewById(R.id.loginExit);

        String[] arrayDomain = new String[]
                { "HOD", "Teacher", "Student", "Messenger", "Admin" };

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,arrayDomain);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDomain.setAdapter(adapter);

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //    addDepartment();

                 //getUser();
               // attemptLogin();
                writeUser();

            }
        });

        mExitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    public void writeUser()
    {

        Call<Users> callToCreateDepartment=apiInterface.insertUsers("umais","umais@gmail.com","umais","1");
        callToCreateDepartment.enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                Log.i(LoginActivity.class.getSimpleName(), response.toString());
                Toast.makeText(LoginActivity.this, "added", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "fail", Toast.LENGTH_SHORT).show();
            }
        });


    }
    public void getUser(){

        Call<List<Users>> call = apiInterface.getUsers();
        call.enqueue(new Callback<List<Users>>(){

            @Override
            public void onResponse(Call<List<Users>> call, Response<List<Users>> response) {
                //Toast.makeText(LoginActivity.this,  response.body().get(0).toString(), Toast.LENGTH_SHORT).show();
                Userss= response.body();
               boolean flagFound=false;
                for (int i=0;i<Userss.size();i++)
                {
                    if(mEmail.getText().toString().equals(Userss.get(i).getUser_email()) &&
                            mPassword.getText().toString().equals(Userss.get(i).getUser_password()) )
                    {
                        flagFound=true;
                        break;
                    }
                    else
                    {
                        flagFound=false;

                    }
                }
                if(flagFound)
                {
                    Intent loginToMain=new Intent(LoginActivity.this,HomeHODActivity.class);
                    startActivity(loginToMain);
                }else
                {
                    Toast.makeText(LoginActivity.this, "User Not Found!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Users>> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "failed on user read :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void addDepartment() {



        Call<Departments> callToCreateDepartment=departmentApiInterface.insertPet("2","SOCA");
        callToCreateDepartment.enqueue(new Callback<Departments>() {
            @Override
            public void onResponse(Call<Departments> call, Response<Departments> response) {
                Log.i(LoginActivity.class.getSimpleName(), response.toString());

                String message = response.body().getName();


                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Departments> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "failed on department write :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });


    }
    private void attemptLogin() {

        //if (mAuthTask != null) {
          //  return;
        //}

        // Reset errors.
        mEmail.setError(null);
        mPassword.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        /*
        if (TextUtils.isEmpty(email)) {
            mEmail.setError("Email Required");
            focusView = mEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmail.setError("Invalid Email");
            focusView = mEmail;
            cancel = true;
        }
        else if (TextUtils.isEmpty(password)) {
            mPassword.setError("Password Required");
            focusView = mPassword;
            cancel = true;
        }
        else if (!isPasswordValid(password)) {
            mPassword.setError("Invalid Password");
            focusView = mPassword;
            cancel = true;
        }

        */
        if (cancel)
        {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //showProgress(true);

            //final ProgressDialog progressDialog = ProgressDialog.show(LoginActivity.this, "Please wait ...", "Logging in ", true);

            if(mDomain.getSelectedItem().toString().equals("HOD"))
            {
                Toast.makeText(getApplicationContext(), "HOD Loaded",Toast.LENGTH_LONG).show();

                Intent intent=new Intent(LoginActivity.this,HomeHODActivity.class);
                startActivity(intent);
            }

            else if(mDomain.getSelectedItem().toString().equals("Teacher"))
            {
                Toast.makeText(getApplicationContext(), "Teacher Loaded",Toast.LENGTH_LONG).show();

                Intent in = new Intent(LoginActivity.this,HomeTeacherActivity.class);
                startActivity(in);
            }
            else if(mDomain.getSelectedItem().toString().equals("Student"))
            {
                Toast.makeText(getApplicationContext(), "Student Loaded",Toast.LENGTH_LONG).show();

                Intent in = new Intent(LoginActivity.this,HomeStudentActivity.class);
                startActivity(in);
            }
            else if(mDomain.getSelectedItem().toString().equals("Messenger"))
            {
                Toast.makeText(getApplicationContext(), "Messenger Loaded",Toast.LENGTH_LONG).show();

                Intent in = new Intent(LoginActivity.this,HomeMessengerActivity.class);
                startActivity(in);
            }

            else if(mDomain.getSelectedItem().toString().equals("Admin"))
            {
                Toast.makeText(getApplicationContext(), "Admin Loaded",Toast.LENGTH_LONG).show();

                Intent in = new Intent(LoginActivity.this,HodAnnouncementActivity.class);
                //Intent in = new Intent(LoginActivity.this,HodTodolistActivity.class);
                startActivity(in);
            }


            //Toast.makeText(getApplicationContext(), "Full Loaded",Toast.LENGTH_LONG).show();
            //new MyTask().execute();
            //mAuthTask = new UserLoginTask(email, password);
            //mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 5;
    }
}


