package com.uol.hodassistant;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class HomeHodFragment extends Fragment {

    CardView homeHodCard01, homeHodCard02, homeHodCard03, homeHodCard04, homeHodCard05, homeHodCard06, homeHodCard07, homeHodCard08, homeHodCard09, homeHodCard10;

    public HomeHodFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_hod, container, false);
    }


    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        homeHodCard01 = (CardView)view.findViewById(R.id.homeHod01);
        homeHodCard02 = (CardView)view.findViewById(R.id.homeHod02);
        homeHodCard03 = (CardView)view.findViewById(R.id.homeHod03);
        homeHodCard04 = (CardView)view.findViewById(R.id.homeHod04);
        homeHodCard05 = (CardView)view.findViewById(R.id.homeHod05);
        homeHodCard06 = (CardView)view.findViewById(R.id.homeHod06);
        homeHodCard07 = (CardView)view.findViewById(R.id.homeHod07);
        homeHodCard08 = (CardView)view.findViewById(R.id.homeHod08);
        homeHodCard09 = (CardView)view.findViewById(R.id.homeHod09);
        homeHodCard10 = (CardView)view.findViewById(R.id.homeHod10);

        homeHodCard01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent1 = new Intent(LoginActivity.this,HodAnnouncementActivity.class);
                //startActivity(intent1);
            }
        });

        homeHodCard07.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent7 = new Intent(getActivity(),HodAnnouncementActivity.class);
                startActivity(intent7);
            }
        });

        homeHodCard09.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent9 = new Intent(getActivity(),HodTodolistActivity.class);
                startActivity(intent9);
            }
        });


    }

}
